package com.itemit.Controller;

import com.itemit.Dao.MockDao;
import com.itemit.Entity.MockItem;
import com.itemit.Entity.MockRequest;
import com.itemit.Entity.ResponseOfRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;


/* I have decided to keep this controller very simple to work with the api.ai POST queries :
 * The query is received in a String, parsed during the instanciation of MockRequest,
 * and processed during the instanciation of ResponseOfRequest.
 */


@RestController
@RequestMapping("/items")
public class MockItemController {

    @Autowired
    private MockDao mockDao;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<MockItem> getAllItems(){
        return mockDao.getAllItems();
    }
/*
    @RequestMapping(value = "/{serial}", method = RequestMethod.GET)
    public MockItem getItemById(@PathVariable("id") int id){
        return mockDao.getItemById(id);
    }
*/
    @RequestMapping(value = "/{serial}", method = RequestMethod.DELETE)
    public void deleteItemById(@PathVariable("id") int id) {
        mockDao.removeItemById(id);
    }

    @RequestMapping(value = "/put", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deleteItemById(@RequestBody MockItem mockItem) {
        mockDao.updateItem(mockItem);
    }


    @RequestMapping(value = "/test", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String processRequest(@RequestBody String requestStr){
        MockRequest mockRequest = new MockRequest(requestStr);
        ResponseOfRequest responseOfRequest = new ResponseOfRequest(mockDao, mockRequest);
        return responseOfRequest.toString();
    }


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insertItem(@RequestBody MockItem mockItem){
        mockDao.insertItem(mockItem);
    }
}