package com.itemit.Dao;

import com.itemit.Entity.MockItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class MockDao {

    @Autowired
    private static HashMap<Integer, MockItem> mockItems;

    static{
        mockItems = new HashMap<Integer, MockItem>(){
            {
                put(1, new MockItem(0, "alex", "computer science"));
                put(2, new MockItem(1, "annanay", "maths"));
                put(3, new MockItem(2, "keith", "physics"));
                put (4, new MockItem(3, "keith", "physics"));
            }
        };
    }

    public Collection<MockItem> getAllItems(){
        return mockItems.values();
    }

    public int itemCountTotal(){
        return mockItems.size();
    }


    public int itemCountWithName(String name){
        int itemCount = 0;
        Iterator<MockItem> i = mockItems.values().iterator();
        while (i.hasNext()){
            if(i.next().getName().equals(name))
                ++itemCount;
        }
        return itemCount;
    }

    public ArrayList<String> getItemsWithSerial(String serial){
        ArrayList<String> UUIDs = new ArrayList<String>();
        Iterator<MockItem> i = mockItems.values().iterator();
        while (i.hasNext()) {
            MockItem mockItem = i.next();
            if(mockItem.getSerial().equals(serial)){
                UUIDs.add(String.valueOf(mockItem.getId()));
            }
        }
        return UUIDs;
    }

    public void updateItem(MockItem mockItem) {
    }


    public void insertItem(MockItem mockItem) {

    }

    public void removeItemById(int id) {
    }
/*
    public com.itemit.Entity.MockItem getItemById(int serial) {
        for (Map.Entry<Integer, MockItem> e: mockItems.entrySet()) {
            Integer s = e.getKey();
            MockItem p = e.getValue();
        }
        return mockItems<s, p>;
    }*/
}