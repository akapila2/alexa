package com.itemit.Entity;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class ResultOfRequest {
    private String source;
    private String resolvedQuery;
    private String action;
    private boolean actionIncomplete;
    private JSONObject parameters;
    private HashMap<String, ContextOfRequest> contexts;
    private MetadataOfRequest metadata;
    private FulfillmentOfRequest fulfillment;
    private double score;

    public ResultOfRequest(JSONObject resultObj) {
        this.source = (String) resultObj.get("source");
        this.resolvedQuery = (String) resultObj.get("resolvedQuery");
        this.action = (String) resultObj.get("action");
        this.actionIncomplete = (boolean) resultObj.get("actionIncomplete");
        this.parameters = (JSONObject) resultObj.get("parameters");
        this.contexts = new HashMap<String, ContextOfRequest>();
        JSONArray contextArray = (JSONArray) resultObj.get("contexts");
        Iterator i = contextArray.iterator();
        while(i.hasNext()){
            JSONObject contextObj = (JSONObject) i.next();
            this.contexts.put((String) contextObj.get("name"), new ContextOfRequest(contextObj));
        }
        this.metadata = new MetadataOfRequest((JSONObject) resultObj.get("metadata"));
        this.fulfillment = new FulfillmentOfRequest((JSONObject) resultObj.get("fulfillment"));
        this.score = ((Number) resultObj.get("score")).doubleValue();
    }

    public MetadataOfRequest getMetadata(){
        return metadata;
    }
    public JSONObject getParameters(){
        return parameters;
    }
}
