package com.itemit.Entity;

import org.json.simple.JSONObject;

public class MessageOfRequest {
    private long type;
    private String speech;

    public MessageOfRequest(JSONObject messageObj){
        this.type = (long) messageObj.get("type");
        this.speech = (String) messageObj.get("speech");
    }

    public String getSpeech(){
        return speech;
    }
}
