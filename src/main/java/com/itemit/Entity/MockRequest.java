package com.itemit.Entity;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Collection;

/* This is the class that parses all the data from the String recieved in the body of the POST query,
 * according to the api.ai format
 */


public class MockRequest {

    private String id;
    private String timestamp;
    private String lang;
    private ResultOfRequest result;
    private StatusOfRequest status;
    private String sessionId;

    public MockRequest(String requestStr){
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(requestStr);

            //this.id = (String) jsonObject.get("id");
            this.timestamp = (String) jsonObject.get("timestamp");
            this.lang = (String) jsonObject.get("lang");
            this.result = new ResultOfRequest((JSONObject) jsonObject.get("result"));
            this.status = new StatusOfRequest((JSONObject) jsonObject.get("status"));
            this.sessionId = (String) jsonObject.get("sessionId");
        } catch(ParseException pe){
            //fill this bit in later (DON'T FORGET!!)
        }
    }

    public MockRequest(){}


    public ResultOfRequest getResult(){
        return result;
    }

}
